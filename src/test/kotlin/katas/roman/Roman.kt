package katas.roman

class Roman {

    fun decode(romanNumbers: String): Int {
        val splitted = romanNumbers.toCharArray()
        var prevSymbol = splitted[0]
        var prevSymbolValue = getSymbolValue(prevSymbol)
        var sum = prevSymbolValue
        for(i in 1..splitted.size - 1){
            val symbol = splitted[i]
            val symbolValue = getSymbolValue(symbol)

            if(prevSymbolValue == symbolValue){
                sum += symbolValue
            } else if(prevSymbolValue < symbolValue){
                sum = symbolValue - sum
            } else {
                sum += symbolValue
            }
            prevSymbol = symbol
            prevSymbolValue = getSymbolValue(prevSymbol)
        }

        return sum
    }

    private fun getSymbolValue(prev: Char): Int {
        return when (prev) {
            'I' -> 1
            'V' -> 5
            'X' -> 10
            else -> 0
        }
    }

}
