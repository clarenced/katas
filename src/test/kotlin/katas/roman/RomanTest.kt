package katas.roman

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe

class RomanTest : ShouldSpec({

    should("return 1 when the symbol is I"){
        val roman = Roman()
        val decoded = roman.decode("I")
        decoded shouldBe  1
    }

    should("return 2 when the symbol is II"){
        val roman = Roman()
        val decoded = roman.decode("II")
        decoded shouldBe 2
    }

    should("return 3 when the symbol is III"){
        val roman = Roman()
        val decoded = roman.decode("III")
        decoded shouldBe 3
    }

    should("return 5 when the symbol is V") {
        val roman = Roman()
        val decoded = roman.decode("V")
        decoded shouldBe 5
    }

    should("return 4 when the symbol is IV") {
        val roman = Roman()
        val decoded = roman.decode("IV")
        decoded shouldBe 4
    }

    should("return 6 when the symbol is VI") {
        val roman = Roman()
        val decoded = roman.decode("VI")
        decoded shouldBe 6
    }

    should("return 7 when the symbol is VII"){
        val roman = Roman()
        val decoded = roman.decode("VII")
        decoded shouldBe 7
    }

    should("return 10 when the symbol is X"){
        val roman = Roman()
        val decoded = roman.decode("X")
        decoded shouldBe 10
    }

    should("return 9 when the symbol is IX") {
        val roman = Roman()
        val decoded = roman.decode("IX")
        decoded shouldBe 9
    }

    should("return 11 when the symbol is XI"){
        val roman = Roman()
        val decoded = roman.decode("XI")
        decoded shouldBe 11
    }

    should("return 31 when the symbol is XXXI"){
        val roman = Roman()
        val decoded = roman.decode("XXXI")
        decoded shouldBe 31
    }

    should("returns 24 when the symbol is XXIV"){
        val roman = Roman()
        val decoded = roman.decode("XXIV")
        decoded shouldBe 24
    }

})