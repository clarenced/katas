package katas.pile.entier

class Stack {

    private var values: ArrayList<Int> = arrayListOf()
    private var currentIndex : Int = 0

    fun count() : Int = values.count()
    fun push(value: Int) = values.add(currentIndex++, value)

    fun pop(): Int = values.removeAt(--currentIndex)

}
