package katas.pile.entier

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe

class StackTest : ShouldSpec( {

    should("returns zero when stack is empty"){
        val stack = Stack()
        stack.count() shouldBe 0
    }

    should("returns a non zero positive value when stack is not empty"){
        val stack = Stack()
        stack.push(55)
        stack.push(40)
        stack.count() shouldBe 2
    }

    should("returns the top of the stack when popping"){
        val stack = Stack()
        stack.push(90)
        stack.push(120)

        val value = stack.pop()
        value shouldBe 120
    }

    should("remove the value of the stack when popping") {
        val stack = Stack()
        stack.push(233)
        stack.push(100)
        stack.pop()
        stack.count() shouldBe 1
    }
})